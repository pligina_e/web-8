/*global $*/
$(document).ready(function () {
    $(".popup_bg").click(function () {
        $(".popup").fadeOut(800);
        history.pushState(null, null, "index.html");
    });
    window.addEventListener("popstate", function () {
        if (location.hash !== "#form") {
            $(".popup").fadeOut(800);
        }
    });
});
function showPopup() {
    history.pushState(null, null, "#form");
    $(".popup").fadeIn(800);
}
document.addEventListener("DOMContentLoaded", function () {
    var button = document.getElementsByTagName("button")[1];
    button.addEventListener("click", function () {
        var letter = {
            name: document.getElementsByName("name"),
            email: document.getElementsByName("email"),
            message: document.getElementsByName("message"),
            check: document.getElementsByName("checkbox")
        };
        fetch("https://formcarry.com/s/Hj7cpRCh8P", {
            method: "post",
            headers: {
                "Content-type": "application/x-www-form-urlencoded;"
            },
            body: JSON.stringify(letter)
        });
        history.pushState(null, null, "index.html");
    });
    [].forEach.call(document.getElementsByTagName("input"), function (item) {
        item.value = localStorage.getItem(item.name);
    });
    [].forEach.call(document.getElementsByTagName("input"), function (item) {
        item.addEventListener("change", function () {
            localStorage.setItem(item.name, item.value);
        });
    });
});